import styles from '../../styles/cats.module.css'
import Link from 'next/link'

export const getStaticProps = async () => {
  const res = await fetch('https://api.thecatapi.com/v1/breeds?api_key=1376ba7e-7041-41c5-bd57-4f1cc5ba86fb&attach_breed=0');
  const data = await res.json();

  return {
    props:{cats: data}
  }
}

const Breeds = ({cats}) => {
  return (
      <div>
          <h1>All Breeds</h1>
          {cats.map(cat => (
            <Link href={'/cat-search/' + cat.name } key={cat.id}>
              <a className={styles.single}>
                <h3>{cat.name}</h3>
               
              </a>
            </Link>
          ))}
      </div>

  );
}

export default Breeds;

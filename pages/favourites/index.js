import styles from '../../styles/cats.module.css'

export const getStaticProps = async () => {
  const res = await fetch('https://api.thecatapi.com/v1/favourites?api_key=1376ba7e-7041-41c5-bd57-4f1cc5ba86fb');
  const data = await res.json();

  return {
    props:{fav: data}
  }
}

const Favourites = ({fav}) => {
  return (
      <div>
          <h1>Favourites</h1>
          {fav.map(cat => (
            <div key={cat.id}>
              <a className={styles.single}>
                <h3>{cat.name}</h3>
              </a>
            </div>
          ))}
      </div>

  );
}

export default Favourites;

import styles from '../../styles/cats.module.css'

export const getStaticPaths = async () => {
    const res = await fetch('https://api.thecatapi.com/v1/breeds?api_key=1376ba7e-7041-41c5-bd57-4f1cc5ba86fb');
    const data = await res.json();

    const paths = data.map(cat => {
        return {
            params: { name: cat.name.toString(), id: cat.id.toString() }
        }
    })
    
    return {
        paths,
        fallback: false
    }
}

export const getStaticProps = async (context) => {
    const _name = context.params.name;
    const _id = context.params.name;
    const res = await fetch('https://api.thecatapi.com/v1/breeds/search?q=' + _id);
    const data = await res.json();
  
    return {
        props:{breed: data}
    }
}

const Breed = ({breed}) => {
   console.log({breed})
    return ( 
        <div>
            <div>
                <h1>{breed[0].name}</h1>
                <p>{breed[0].description}</p>
                <img src={breed[0].url} width="200px"/>
                <img className={styles.imagestyle} src={`https://cdn2.thecatapi.com/images/${breed[0].reference_image_id}.jpg`}/>
            </div>
        </div>
     );
}
 
export default Breed;
import styles from '../../styles/cats.module.css'

export const getStaticPaths = async () => {
    const res = await fetch('https://api.thecatapi.com/v1/categories?api_key=1376ba7e-7041-41c5-bd57-4f1cc5ba86fb');
    const data = await res.json();

    const paths = data.map(cat => {
        return {
            params: {id: cat.id.toString() }
        }
    })
    
    return {
        paths,
        fallback: false
    }
}

export const getStaticProps = async (context) => {
    const _id = context.params.name;
    const res = await fetch('https://api.thecatapi.com/v1/images/search?category_ids=1');
    const data = await res.json();
  
    return {
        props:{breed: data}
    }
}

const Category = ({breed}) => {
    console.log({breed})
     return ( 
        <div>
            <h1>{breed[0].categories[0].name}</h1>
            <img src={breed[0].url} width="200px"/>
        </div>
      );
 }
  
 export default Category;
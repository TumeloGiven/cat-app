import styles from '../../styles/cats.module.css'
import Link from 'next/link'

export const getStaticProps = async () => {
  const res = await fetch('https://api.thecatapi.com/v1/categories?api_key=1376ba7e-7041-41c5-bd57-4f1cc5ba86fb');
  const data = await res.json();

  return {
    props:{categories: data}
  }
}

const Categories = ({categories}) => {
  return (
      <div>
          <h1>Categories</h1>
          {categories.map(cat => (
            <Link href={'/categories/' + cat.id} key={cat.id}>
              <a className={styles.single}>
                <h3>{cat.name}</h3>
              </a>
            </Link>
          ))}
      </div>

  );
}

export default Categories;

import Head from 'next/head'
import Navbar from '../comps/Navbar'
import Footer from '../comps/Footer'
import Image from 'next/image'
import styles from '../styles/Home.module.css'

export const getStaticProps = async () => {
  const res = await fetch('https://api.thecatapi.com/v1/breeds?api_key=1376ba7e-7041-41c5-bd57-4f1cc5ba86fb&attach_breed=0');
  const data = await res.json();

  if (!data) {
      return {
         notFound: true,
      }
   }
   
  return {
    props:{cats: data}
  }
}

export default function Home({cats}) {
  console.log({cats})
  return (
    <div className={styles.imagecontainer}>
      {cats.map(cat => (
            <div className={styles.imagecard} href={'/cat-search/' + cat.name } key={cat.id}>
              <a className={styles.single}>
                <h3 className={styles.imgHeading}>{cat.name}</h3>
              </a>
                <button className={styles.favBtn} type="button">Favourite</button>
              {cat.image && <img src={cat.image.url} width="100%"/>}
            </div>
          ))}
    </div>
  )
}

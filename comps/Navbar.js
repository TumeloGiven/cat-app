import Link from 'next/link'

const Navbar = () => {
    return (
        <nav>
            <div className="logo">
                <h1>Cat APP</h1>
            </div>
            <Link href="/breeds"><a>Breeds</a></Link>
            <Link href="/categories"><a>Categories</a></Link>
            <Link href="/favourites"><a>Favourites</a></Link>
        </nav>
    );
  }
  
  export default Navbar;
  